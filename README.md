## Example nodejs project

How to use:

1. Clone this repository
1. ```nix-build```
1. ```NODE_PATH=./result/lib/node_modules/ node```
1. ```require ('websocket')```

The ```nix-build``` command builds the derivation specified in default.nix and
puts it into your nix store (see ```nix-store``` for more information) which is
useful for deployments and distributed computing. ```./result``` directory is
just a symlink to this derivation, and we can use it to develop the project.
